//
//  GSADB.h
//  GSAuth
//
//  Created by Dan Kalinin on 12/12/19.
//

#import "GSAMain.h"
#import "GSAAccount.h"
#import "GSADomain.h"
#import "GSAController.h"
#import "GSAInvitation.h"

@interface GSADatabase : NSEObject

@property (readonly) NSArray<GSAAccount *> *accounts;
@property (readonly) NSArray<GSADomain *> *domains;
@property (readonly) NSArray<GSAController *> *controllers;
@property (readonly) NSArray<GSAInvitation *> *invitations;

- (GSAAccount *)account:(NSString *)phone;
- (GSADomain *)domain:(NSString *)id;
- (GSADomain *)domain:(NSString *)id account:(NSString *)phone;
- (GSADomain *)domain:(NSString *)id owner:(NSString *)phone;
- (GSAController *)controller:(NSString *)mac;
- (GSAController *)controller:(NSString *)mac account:(NSString *)phone;
- (GSAController *)controller:(NSString *)mac owner:(NSString *)phone;
- (GSAController *)controller:(NSString *)mac domain:(NSString *)id;
- (GSAInvitation *)invitation:(NSString *)id;

- (NSArray<GSADomain *> *)domainsForAccount:(NSString *)phone;
- (NSArray<GSADomain *> *)domainsForOwner:(NSString *)phone;
- (NSArray<GSAController *> *)controllersForDomain:(NSString *)id;
- (NSArray<GSAInvitation *> *)invitationsForDomain:(NSString *)id;
- (NSArray<GSAAccount *> *)accountsForDomain:(NSString *)id;

- (void)deleteController:(NSString *)mac;

+ (instancetype)shared;

@end
