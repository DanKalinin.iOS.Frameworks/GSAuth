//
//  GSAError.h
//  FoundationExt
//
//  Created by Dan Kalinin on 12/12/19.
//

#import "GSAMain.h"

extern NSErrorDomain GSAErrorDomain;

@interface GSAError : NSEError

- (instancetype)initWithSDKError:(GSASDKError *)sdkError;

@end
