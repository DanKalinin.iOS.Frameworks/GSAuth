//
//  GSAController.m
//  GSAuth
//
//  Created by Dan Kalinin on 12/12/19.
//

#import "GSAController.h"
#import "GSADomain.h"
#import "GSADatabase.h"










@implementation GSAController {
    NSString *__domain;
    GSADomain *_domain;
}

- (GSADomain *)domain {
    if (_domain == nil) {
        _domain = [GSADatabase.shared domain:__domain];
    }
    
    return _domain;
}

- (instancetype)initWithMAC:(NSString *)mac serial:(NSString *)serial model:(NSString *)model key:(NSString *)key ip:(NSString *)ip port:(NSUInteger)port bssid:(NSString *)bssid {
    self = super.init;
    
    if (self != nil) {
        _mac = mac;
        _serial = serial;
        _model = model;
        _key = key;
        _ip = ip;
        _port = port;
        _bssid = bssid;
    }
    
    return self;
}

- (instancetype)initWithSDKController:(GSASDKController *)sdkController {
    self = super.init;
    
    if (self != nil) {
        _id = @(sdkController->id);
        _mac = @(sdkController->mac);
        _serial = @(sdkController->serial);
        _model = @(sdkController->model);
        _key = @(sdkController->key);
        _verified = sdkController->verified;
        _ip = @(sdkController->ip);
        _port = sdkController->port;
        _bssid = @(sdkController->bssid);
        __domain = @(sdkController->domain);
        
        if (sdkController->token != NULL) {
            _token = [GSAToken.alloc initWithSDKToken:sdkController->token];
        }
    }
    
    return self;
}

- (void)toSDKController:(GSASDKController *)sdkController {
    sdkController->mac = (char *)_mac.UTF8String;
    sdkController->serial = (char *)_serial.UTF8String;
    sdkController->model = (char *)_model.UTF8String;
    sdkController->key = (char *)_key.UTF8String;
    sdkController->ip = (char *)_ip.UTF8String;
    sdkController->port = (int)_port;
    sdkController->bssid = (char *)_bssid.UTF8String;
}

@end










@implementation NSArray (GSAController)

+ (instancetype)gsaControllersWithSDKControllers:(GSASDKControllers *)sdkControllers {
    NSMutableArray *ret = NSMutableArray.array;
    
    for (int i = 0; i < sdkControllers->n; i++) {
        GSASDKController *sdkController = sdkControllers->data[i];
        GSAController *controller = [GSAController.alloc initWithSDKController:sdkController];
        [ret addObject:controller];
    }
    
    return ret;
}

- (void)gsaToSDKControllers:(GSASDKControllers *)sdkControllers {
    for (NSUInteger i = 0; i < self.count; i++) {
        GSAController *controller = self[i];
        GSASDKController *sdkController = sdkControllers->data[i];
        [controller toSDKController:sdkController];
    }
}

@end
