//
//  GSAError.m
//  FoundationExt
//
//  Created by Dan Kalinin on 12/12/19.
//

#import "GSAError.h"

NSErrorDomain GSAErrorDomain = @"GSAErrorDomain";

@implementation GSAError

- (instancetype)initWithSDKError:(GSASDKError *)sdkError {
    NSBundle *bundle = [NSBundle bundleForClass:self.class];
    
    NSMutableDictionary *info = NSMutableDictionary.dictionary;
    info[NSLocalizedDescriptionKey] = [bundle localizedStringForKey:@(sdkError->message) value:nil table:nil];
    self = [super initWithDomain:GSAErrorDomain code:sdkError->code userInfo:info];
    return self;
}

@end
