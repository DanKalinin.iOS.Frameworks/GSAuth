//
//  GSAController.h
//  GSAuth
//
//  Created by Dan Kalinin on 12/12/19.
//

#import "GSAMain.h"
#import "GSAToken.h"

@class GSADomain;










@interface GSAController : NSEObject

@property (readonly) NSString *id;
@property (readonly) NSString *mac;
@property (readonly) NSString *serial;
@property (readonly) NSString *model;
@property (readonly) NSString *key;
@property (readonly) BOOL verified;
@property (readonly) NSString *ip;
@property (readonly) NSUInteger port;
@property (readonly) NSString *bssid;
@property (readonly) GSAToken *token;
@property (readonly) GSADomain *domain;

- (instancetype)initWithMAC:(NSString *)mac serial:(NSString *)serial model:(NSString *)model key:(NSString *)key ip:(NSString *)ip port:(NSUInteger)port bssid:(NSString *)bssid;
- (instancetype)initWithSDKController:(GSASDKController *)sdkController;
- (void)toSDKController:(GSASDKController *)sdkController;

@end










@interface NSArray (GSAController)

+ (instancetype)gsaControllersWithSDKControllers:(GSASDKControllers *)sdkControllers;
- (void)gsaToSDKControllers:(GSASDKControllers *)sdkControllers;

@end
