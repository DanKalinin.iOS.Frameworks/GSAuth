//
//  GSAAccount.h
//  GSAuth
//
//  Created by Dan Kalinin on 12/12/19.
//

#import "GSAMain.h"
#import "GSAToken.h"

@class GSADomain;










@interface GSAAccount : NSEObject

@property (readonly) NSString *id;
@property (readonly) NSString *name;
@property (readonly) NSString *phone;
@property (readonly) NSDate *expiration;
@property (readonly) GSAToken *token;
@property (readonly) NSArray<GSADomain *> *domains;

- (instancetype)initWithSDKAccount:(GSASDKAccount *)sdkAccount;

@end










@interface NSArray (GSAAccount)

+ (instancetype)gsaAccountsWithSDKAccounts:(GSASDKAccounts *)sdkAccounts;

@end
