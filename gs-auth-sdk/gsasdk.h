//
// Created by dan on 15.11.2019.
//

#ifndef LIBRARY_GS_AUTH_GSASDK_H
#define LIBRARY_GS_AUTH_GSASDK_H

#define GSA_SDK_BASE_DEBUG "https://smart-home-cloud.test.gs-labs.tv"
#define GSA_SDK_BASE_PRODUCTION "https://demo-smh.gs-labs.ru"

#define GSA_SDK_IDENTITY_PHONE "phone"
#define GSA_SDK_IDENTITY_EMAIL "email"

typedef enum {
    GSA_SDK_ERROR_FAILED
} GSASDKErrorCode;

typedef struct {
    GSASDKErrorCode code;
    char *message;
} GSASDKError;

typedef struct {
    char *access;
    char *refresh;
} GSASDKToken;

typedef struct {
    char *id;
    char *name;
    char *phone;
    long expiration;
    GSASDKToken *token;
} GSASDKAccount;

typedef struct {
    GSASDKAccount **data;
    int n;
} GSASDKAccounts;

typedef struct {
    char *id;
    char *name;
    char *account;
    char *owner;
    GSASDKToken *token;
} GSASDKDomain;

typedef struct {
    GSASDKDomain **data;
    int n;
} GSASDKDomains;

typedef struct {
    char *id;
    char *domain;
    char *mac;
    char *serial;
    char *model;
    char *key;
    int verified;
    char *ip;
    int port;
    char *bssid;
    GSASDKToken *token;
} GSASDKController;

typedef struct {
    GSASDKController **data;
    int n;
} GSASDKControllers;

typedef struct {
    char *id;
    char *name;
    int expiration;
    int access;
} GSASDKInvitation;

typedef struct {
    GSASDKInvitation **data;
    int n;
} GSASDKInvitations;

void gsa_sdk_error_free(GSASDKError *self);
void gsa_sdk_token_free(GSASDKToken *self);
void gsa_sdk_account_free(GSASDKAccount *self);
void gsa_sdk_accounts_free(GSASDKAccounts *self);
void gsa_sdk_domain_free(GSASDKDomain *self);
void gsa_sdk_domains_free(GSASDKDomains *self);
void gsa_sdk_controller_free(GSASDKController *self);
void gsa_sdk_controllers_free(GSASDKControllers *self);
void gsa_sdk_invitation_free(GSASDKInvitation *self);
void gsa_sdk_invitations_free(GSASDKInvitations *self);

void gsa_sdk_run(void);

GSASDKAccounts *gsa_sdk_accounts(void);
GSASDKAccount *gsa_sdk_account(char *phone);
GSASDKDomains *gsa_sdk_account_domains(char *phone);

GSASDKDomains *gsa_sdk_domains(void);
GSASDKDomain *gsa_sdk_domain(char *id);
GSASDKControllers *gsa_sdk_domain_controllers(char *id);
GSASDKAccounts *gsa_sdk_domain_accounts(char *id);

GSASDKControllers *gsa_sdk_controllers(void);
GSASDKController *gsa_sdk_controller(char *mac);
void gsa_sdk_controller_delete(char *mac);

GSASDKInvitations *gsa_sdk_invitations(void);
GSASDKInvitation *gsa_sdk_invitation(char *id);

typedef void (*GSASDKPhoneCheckCallback)(int available, GSASDKError *error, void *data);
typedef void (*GSASDKAccountLoginCallback)(GSASDKAccount *account, GSASDKError *error, void *data);
typedef void (*GSASDKAccountCreateCallback)(GSASDKAccount *account, GSASDKError *error, void *data);
typedef void (*GSASDKAccountVerifyCallback)(GSASDKError *error, void *data);
typedef void (*GSASDKAccountVerifyV1Callback)(GSASDKAccount *account, GSASDKError *error, void *data);
typedef void (*GSASDKAccountRequestCodeCallback)(GSASDKError *error, void *data);
typedef void (*GSASDKAccountRequestCodeV1Callback)(GSASDKError *error, void *data);
typedef void (*GSASDKAccountPasswordUpdateCallback)(GSASDKError *error, void *data);
typedef void (*GSASDKAccountGetCallback)(GSASDKError *error, void *data);
typedef void (*GSASDKAccountDomainCreateCallback)(GSASDKDomain *domain, GSASDKError *error, void *data);
typedef void (*GSASDKAccountDomainVerifyCallback)(GSASDKError *error, void *data);
typedef void (*GSASDKAccountControllerVerifyCallback)(GSASDKError *error, void *data);
typedef void (*GSASDKAccountInvitationAcceptCallback)(GSASDKDomain *domain, GSASDKError *error, void *data);
typedef void (*GSASDKAccountDomainDeleteCallback)(GSASDKError *error, void *data);
typedef void (*GSASDKAccountRefreshCallback)(GSASDKError *error, void *data);
typedef void (*GSASDKAccountLogoutCallback)(GSASDKError *error, void *data);
typedef void (*GSASDKDomainDeleteCallback)(GSASDKError *error, void *data);
typedef void (*GSASDKDomainRenameCallback)(GSASDKError *error, void *data);
typedef void (*GSASDKDomainGetCodeCallback)(char *code, GSASDKError *error, void *data);
typedef void (*GSASDKDomainControllersAddCallback)(GSASDKError *error, void *data);
typedef void (*GSASDKDomainControllerDeleteCallback)(GSASDKError *error, void *data);
typedef void (*GSASDKDomainInvitationCreateCallback)(GSASDKInvitation *invitation, GSASDKError *error, void *data);
typedef void (*GSASDKDomainAccountDeleteCallback)(GSASDKError *error, void *data);

void gsa_sdk_phone_check(char *phone, GSASDKPhoneCheckCallback callback, void *data);
void gsa_sdk_account_login(char *phone, char *password, GSASDKAccountLoginCallback callback, void *data);
void gsa_sdk_account_create(char *phone, char *password, char *name, GSASDKAccountCreateCallback callback, void *data);
void gsa_sdk_account_verify(char *phone, char *code, GSASDKAccountVerifyCallback callback, void *data);
void gsa_sdk_account_verify_v1(char *phone, char *code, GSASDKAccountVerifyV1Callback callback, void *data);
void gsa_sdk_account_request_code(char *phone, GSASDKAccountRequestCodeCallback callback, void *data);
void gsa_sdk_account_request_code_v1(char *phone, GSASDKAccountRequestCodeV1Callback callback, void *data);
void gsa_sdk_account_password_update(char *phone, char *password, GSASDKAccountPasswordUpdateCallback callback, void *data);
void gsa_sdk_account_get(char *phone, GSASDKAccountGetCallback callback, void *data);
void gsa_sdk_account_domain_create(char *phone, char *name, GSASDKControllers *controllers, GSASDKAccountDomainCreateCallback callback, void *data);
void gsa_sdk_account_domain_verify(char *phone, char *id, GSASDKAccountDomainVerifyCallback callback, void *data);
void gsa_sdk_account_controller_verify(char *phone, char *mac, GSASDKAccountControllerVerifyCallback callback, void *data);
void gsa_sdk_account_invitation_accept(char *phone, char *id, GSASDKAccountInvitationAcceptCallback callback, void *data);
void gsa_sdk_account_domain_delete(char *phone, char *id, GSASDKAccountDomainDeleteCallback callback, void *data);
void gsa_sdk_account_refresh(char *phone, GSASDKAccountRefreshCallback callback, void *data);
void gsa_sdk_account_logout(char *phone, GSASDKAccountLogoutCallback callback, void *data);
void gsa_sdk_domain_delete(char *phone, char *id, GSASDKDomainDeleteCallback callback, void *data);
void gsa_sdk_domain_rename(char *phone, char *id, char *name, GSASDKDomainRenameCallback callback, void *data);
void gsa_sdk_domain_get_code(char *phone, char *id, GSASDKDomainGetCodeCallback callback, void *data);
void gsa_sdk_domain_controllers_add(char *phone, char *id, GSASDKControllers *controllers, GSASDKDomainControllersAddCallback callback, void *data);
void gsa_sdk_domain_controller_delete(char *phone, char *id, char *mac, GSASDKDomainControllerDeleteCallback callback, void *data);
void gsa_sdk_domain_invitation_create(char *phone, char *id, char *name, int expiration, int access, GSASDKDomainInvitationCreateCallback callback, void *data);
void gsa_sdk_domain_account_delete(char *phone, char *id, char *guest_phone, GSASDKDomainAccountDeleteCallback callback, void *data);

#endif //LIBRARY_GS_AUTH_GSASDK_H
