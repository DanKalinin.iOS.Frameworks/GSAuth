//
//  GSADomain.m
//  GSAuth
//
//  Created by Dan Kalinin on 12/12/19.
//

#import "GSADomain.h"
#import "GSAAccount.h"
#import "GSAController.h"
#import "GSADatabase.h"










@implementation GSADomain {
    NSString *__account;
    NSString *__owner;
    GSAAccount *_account;
    GSAAccount *_owner;
    NSArray<GSAController *> *_controllers;
    NSArray<GSAAccount *> *_accounts;
}

- (GSAAccount *)account {
    if (_account == nil) {
        _account = [GSADatabase.shared account:__account];
    }
    
    return _account;
}

- (GSAAccount *)owner {
    if (_owner == nil) {
        _owner = [GSADatabase.shared account:__owner];
    }
    
    return _owner;
}

- (NSArray<GSAController *> *)controllers {
    if (_controllers == nil) {
        _controllers = [GSADatabase.shared controllersForDomain:_id];
    }
    
    return _controllers;
}

- (NSArray<GSAAccount *> *)accounts {
    if (_accounts == nil) {
        _accounts = [GSADatabase.shared accountsForDomain:_id];
    }
    
    return _accounts;
}

- (instancetype)initWithSDKDomain:(GSASDKDomain *)sdkDomain {
    self = super.init;
    
    if (self != nil) {
        _id = @(sdkDomain->id);
        _name = @(sdkDomain->name);
        __account = @(sdkDomain->account);
        __owner = @(sdkDomain->owner);
        
        if (sdkDomain->token != NULL) {
            _token = [GSAToken.alloc initWithSDKToken:sdkDomain->token];
        }
    }
    
    return self;
}

@end










@implementation NSArray (GSADomain)

+ (instancetype)gsaDomainsWithSDKDomains:(GSASDKDomains *)sdkDomains {
    NSMutableArray *ret = NSMutableArray.array;
    
    for (int i = 0; i < sdkDomains->n; i++) {
        GSASDKDomain *sdkDomain = sdkDomains->data[i];
        GSADomain *domain = [GSADomain.alloc initWithSDKDomain:sdkDomain];
        [ret addObject:domain];
    }
    
    return ret;
}

@end
