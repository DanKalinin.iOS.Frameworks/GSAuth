//
//  GSAClient.h
//  Pods
//
//  Created by Dan Kalinin on 12/12/19.
//

#import "GSAMain.h"
#import "GSAError.h"
#import "GSAAccount.h"
#import "GSADomain.h"
#import "GSAController.h"
#import "GSAInvitation.h"

typedef void (^GSAClientPhoneCheckCompletion)(BOOL available, GSAError *error);
typedef void (^GSAClientAccountLoginCompletion)(GSAAccount *account, GSAError *error);
typedef void (^GSAClientAccountCreateCompletion)(GSAAccount *account, GSAError *error);
typedef void (^GSAClientAccountVerifyCompletion)(GSAError *error);
typedef void (^GSAClientAccountVerifyV1Completion)(GSAAccount *account, GSAError *error);
typedef void (^GSAClientAccountRequestCodeCompletion)(GSAError *error);
typedef void (^GSAClientAccountRequestCodeV1Completion)(GSAError *error);
typedef void (^GSAClientAccountPasswordUpdateCompletion)(GSAError *error);
typedef void (^GSAClientAccountGetCompletion)(GSAError *error);
typedef void (^GSAClientAccountDomainCreateCompletion)(GSADomain *domain, GSAError *error);
typedef void (^GSAClientAccountDomainVerifyCompletion)(GSAError *error);
typedef void (^GSAClientAccountControllerVerifyCompletion)(GSAError *error);
typedef void (^GSAClientAccountInvitationAcceptCompletion)(GSADomain *domain, GSAError *error);
typedef void (^GSAClientAccountDomainDeleteCompletion)(GSAError *error);
typedef void (^GSAClientAccountRefreshCompletion)(GSAError *error);
typedef void (^GSAClientAccountLogoutCompletion)(GSAError *error);
typedef void (^GSAClientDomainDeleteCompletion)(GSAError *error);
typedef void (^GSAClientDomainRenameCompletion)(GSAError *error);
typedef void (^GSAClientDomainGetCodeCompletion)(NSString *code, GSAError *error);
typedef void (^GSAClientDomainControllersAddCompletion)(GSAError *error);
typedef void (^GSAClientDomainControllerDeleteCompletion)(GSAError *error);
typedef void (^GSAClientDomainInvitationCreateCompletion)(GSAInvitation *invitation, GSAError *error);
typedef void (^GSAClientDomainAccountDeleteCompletion)(GSAError *error);

@interface GSAClient : NSEObject

- (void)phoneCheck:(NSString *)phone completion:(GSAClientPhoneCheckCompletion)completion;
- (void)accountLogin:(NSString *)phone password:(NSString *)password completion:(GSAClientAccountLoginCompletion)completion;
- (void)accountCreate:(NSString *)phone password:(NSString *)password name:(NSString *)name completion:(GSAClientAccountCreateCompletion)completion;
- (void)accountVerify:(NSString *)phone code:(NSString *)code completion:(GSAClientAccountVerifyCompletion)completion;
- (void)accountVerifyV1:(NSString *)phone code:(NSString *)code completion:(GSAClientAccountVerifyV1Completion)completion;
- (void)accountRequestCode:(NSString *)phone completion:(GSAClientAccountRequestCodeCompletion)completion;
- (void)accountRequestCodeV1:(NSString *)phone completion:(GSAClientAccountRequestCodeV1Completion)completion;
- (void)accountPasswordUpdate:(NSString *)phone password:(NSString *)password completion:(GSAClientAccountPasswordUpdateCompletion)completion;
- (void)accountGet:(NSString *)phone completion:(GSAClientAccountGetCompletion)completion;
- (void)accountDomainCreate:(NSString *)phone name:(NSString *)name controllers:(NSArray<GSAController *> *)controllers completion:(GSAClientAccountDomainCreateCompletion)completion;
//- (void)accountDomainVerify:(NSString *)phone id:(NSString *)id completion:(GSAClientAccountDomainVerifyCompletion)completion;
//- (void)accountControllerVerify:(NSString *)phone mac:(NSString *)mac completion:(GSAClientAccountControllerVerifyCompletion)completion;
- (void)accountInvitationAccept:(NSString *)phone id:(NSString *)id completion:(GSAClientAccountInvitationAcceptCompletion)completion;
- (void)accountDomainDelete:(NSString *)phone id:(NSString *)id completion:(GSAClientAccountDomainDeleteCompletion)completion;
- (void)accountRefresh:(NSString *)phone completion:(GSAClientAccountRefreshCompletion)completion;
- (void)accountLogout:(NSString *)phone completion:(GSAClientAccountLogoutCompletion)completion;
- (void)account:(NSString *)phone domainDelete:(NSString *)id completion:(GSAClientDomainDeleteCompletion)completion;
- (void)account:(NSString *)phone domainRename:(NSString *)id name:(NSString *)name completion:(GSAClientDomainRenameCompletion)completion;
- (void)account:(NSString *)phone getCode:(NSString *)id completion:(GSAClientDomainGetCodeCompletion)completion;
- (void)account:(NSString *)phone domainControllersAdd:(NSString *)id controllers:(NSArray<GSAController *> *)controllers completion:(GSAClientDomainControllersAddCompletion)completion;
- (void)account:(NSString *)phone domainControllerDelete:(NSString *)id mac:(NSString *)mac completion:(GSAClientDomainControllerDeleteCompletion)completion;
- (void)account:(NSString *)phone domainInvitationCreate:(NSString *)id name:(NSString *)name expiration:(NSUInteger)expiration access:(NSUInteger)access completion:(GSAClientDomainInvitationCreateCompletion)completion;
- (void)account:(NSString *)phone domainGuestDelete:(NSString *)id guestPhone:(NSString *)guestPhone completion:(GSAClientDomainAccountDeleteCompletion)completion;

+ (instancetype)shared;

@end
