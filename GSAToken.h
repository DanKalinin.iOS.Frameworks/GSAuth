//
//  GSAToken.h
//  GSAuth
//
//  Created by Dan Kalinin on 12/12/19.
//

#import "GSAMain.h"

@interface GSAToken : NSEObject

@property NSString *access;
@property NSString *refresh;

- (instancetype)initWithSDKToken:(GSASDKToken *)token;
- (void)toSDKToken:(GSASDKToken *)token;

@end
