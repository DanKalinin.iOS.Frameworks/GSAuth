//
//  GSAInvitation.h
//  GSAuth
//
//  Created by Dan Kalinin on 4/1/20.
//

#import "GSAMain.h"

@class GSADomain;










@interface GSAInvitation : NSEObject

@property (readonly) NSString *id;
@property (readonly) NSString *name;
@property (readonly) NSUInteger expiration;
@property (readonly) NSUInteger access;
@property (readonly) GSADomain *domain;

- (instancetype)initWithSDKInvitation:(GSASDKInvitation *)sdkInvitation;

@end










@interface NSArray (GSAInvitation)

+ (instancetype)gsaInvitationsWithSDKInvitations:(GSASDKInvitations *)sdkInvitations;

@end
