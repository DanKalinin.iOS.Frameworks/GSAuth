//
//  GSADB.m
//  GSAuth
//
//  Created by Dan Kalinin on 12/12/19.
//

#import "GSADatabase.h"

@implementation GSADatabase

- (NSArray<GSAAccount *> *)accounts {
    GSASDKAccounts *sdkAccounts = gsa_sdk_accounts();
    NSArray *ret = [NSArray gsaAccountsWithSDKAccounts:sdkAccounts];
    gsa_sdk_accounts_free(sdkAccounts);
    return ret;
}

- (NSArray<GSADomain *> *)domains {
    GSASDKDomains *sdkDomains = gsa_sdk_domains();
    NSArray *ret = [NSArray gsaDomainsWithSDKDomains:sdkDomains];
    gsa_sdk_domains_free(sdkDomains);
    return ret;
}

- (NSArray<GSAController *> *)controllers {
    GSASDKControllers *sdkControllers = gsa_sdk_controllers();
    NSArray *ret = [NSArray gsaControllersWithSDKControllers:sdkControllers];
    gsa_sdk_controllers_free(sdkControllers);
    return ret;
}

- (NSArray<GSAInvitation *> *)invitations {
    GSASDKInvitations *sdkInvitations = gsa_sdk_invitations();
    NSArray *ret = [NSArray gsaInvitationsWithSDKInvitations:sdkInvitations];
    gsa_sdk_invitations_free(sdkInvitations);
    return ret;
}

- (GSAAccount *)account:(NSString *)phone {
    GSAAccount *ret = nil;
    
    GSASDKAccount *sdkAccount = gsa_sdk_account((char *)phone.UTF8String);
    if (sdkAccount != NULL) {
        ret = [GSAAccount.alloc initWithSDKAccount:sdkAccount];
        gsa_sdk_account_free(sdkAccount);
    }
    
    return ret;
}

- (GSADomain *)domain:(NSString *)id {
    GSADomain *ret = nil;
    
    GSASDKDomain *sdkDomain = gsa_sdk_domain((char *)id.UTF8String);
    if (sdkDomain != NULL) {
        ret = [GSADomain.alloc initWithSDKDomain:sdkDomain];
        gsa_sdk_domain_free(sdkDomain);
    }
    
    return ret;
}

- (GSADomain *)domain:(NSString *)id account:(NSString *)phone {
    GSADomain *ret = [self domain:id];
    
    if (![ret.account.phone isEqualToString:phone]) {
        ret = nil;
    }
    
    return ret;
}

- (GSADomain *)domain:(NSString *)id owner:(NSString *)phone {
    GSADomain *ret = [self domain:id];
    
    if (![ret.owner.phone isEqualToString:phone]) {
        ret = nil;
    }
    
    return ret;
}

- (GSAController *)controller:(NSString *)mac {
    GSAController *ret = nil;
    
    GSASDKController *sdkController = gsa_sdk_controller((char *)mac.UTF8String);
    if (sdkController != NULL) {
        ret = [GSAController.alloc initWithSDKController:sdkController];
        gsa_sdk_controller_free(sdkController);
    }
    
    return ret;
}

- (GSAController *)controller:(NSString *)mac account:(NSString *)phone {
    GSAController *ret = [self controller:mac];
    
    if (![ret.domain.account.phone isEqualToString:phone]) {
        ret = nil;
    }
    
    return ret;
}

- (GSAController *)controller:(NSString *)mac owner:(NSString *)phone {
    GSAController *ret = [self controller:mac];
    
    if (![ret.domain.owner.phone isEqualToString:phone]) {
        ret = nil;
    }
    
    return ret;
}

- (GSAController *)controller:(NSString *)mac domain:(NSString *)id {
    GSAController *ret = [self controller:mac];
    
    if (![ret.domain.id isEqualToString:id]) {
        ret = nil;
    }
    
    return ret;
}

- (GSAInvitation *)invitation:(NSString *)id {
    GSAInvitation *ret = nil;
    
    GSASDKInvitation *sdkInvitation = gsa_sdk_invitation((char *)id.UTF8String);
    if (sdkInvitation != NULL) {
        ret = [GSAInvitation.alloc initWithSDKInvitation:sdkInvitation];
        gsa_sdk_invitation_free(sdkInvitation);
    }
    
    return ret;
}

- (NSArray<GSADomain *> *)domainsForAccount:(NSString *)phone {
    GSASDKDomains *sdkDomains = gsa_sdk_account_domains((char *)phone.UTF8String);
    NSArray *ret = [NSArray gsaDomainsWithSDKDomains:sdkDomains];
    gsa_sdk_domains_free(sdkDomains);
    return ret;
}

- (NSArray<GSADomain *> *)domainsForOwner:(NSString *)phone; {
    NSArray *ret = [self domainsForAccount:phone];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"owner.phone == %@", phone];
    ret = [ret filteredArrayUsingPredicate:predicate];
    return ret;
}

- (NSArray<GSAController *> *)controllersForDomain:(NSString *)id {
    GSASDKControllers *sdkControllers = gsa_sdk_domain_controllers((char *)id.UTF8String);
    NSArray *ret = [NSArray gsaControllersWithSDKControllers:sdkControllers];
    gsa_sdk_controllers_free(sdkControllers);
    return ret;
}

- (NSArray<GSAInvitation *> *)invitationsForDomain:(NSString *)id {
//    GSASDKInvitations *sdkInvitations = gsa_sdk_domain_invitations((char *)id.UTF8String);
//    NSArray *ret = [NSArray gsaInvitationsWithSDKInvitations:sdkInvitations];
//    gsa_sdk_invitations_free(sdkInvitations);
//    return ret;
    return nil;
}

- (NSArray<GSAAccount *> *)accountsForDomain:(NSString *)id {
    GSASDKAccounts *sdkAccounts = gsa_sdk_domain_accounts((char *)id.UTF8String);
    NSArray *ret = [NSArray gsaAccountsWithSDKAccounts:sdkAccounts];
    gsa_sdk_accounts_free(sdkAccounts);
    return ret;
}

- (void)deleteController:(NSString *)mac {
    gsa_sdk_controller_delete((char *)mac.UTF8String);
}

+ (instancetype)shared {
    static GSADatabase *ret = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ret = self.new;
    });
    
    return ret;
}

@end
