//
//  GSAToken.m
//  GSAuth
//
//  Created by Dan Kalinin on 12/12/19.
//

#import "GSAToken.h"

@implementation GSAToken

- (instancetype)initWithSDKToken:(GSASDKToken *)token {
    self = super.init;
    
    if (self != nil) {
        _access = @(token->access);
        _refresh = @(token->refresh);
    }
    
    return self;
}

- (void)toSDKToken:(GSASDKToken *)token {
    token->access = (char *)_access.UTF8String;
    token->refresh = (char *)_refresh.UTF8String;
}

@end
