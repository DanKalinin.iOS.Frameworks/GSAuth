//
//  GSAAccount.m
//  GSAuth
//
//  Created by Dan Kalinin on 12/12/19.
//

#import "GSAAccount.h"
#import "GSADomain.h"
#import "GSADatabase.h"










@implementation GSAAccount {
    NSArray<GSADomain *> *_domains;
}

- (NSArray<GSADomain *> *)domains {
    if (_domains == nil) {
        _domains = [GSADatabase.shared domainsForAccount:_phone];
    }
    
    return _domains;
}

- (instancetype)initWithSDKAccount:(GSASDKAccount *)sdkAccount {
    self = super.init;
    
    if (self != nil) {
        _id = @(sdkAccount->id);
        _name = @(sdkAccount->name);
        _phone = @(sdkAccount->phone);
        _expiration = [NSDate dateWithTimeIntervalSince1970:sdkAccount->expiration];
        
        if (sdkAccount->token != NULL) {
            _token = [GSAToken.alloc initWithSDKToken:sdkAccount->token];
        }
    }
    
    return self;
}

+ (NSArray *)accountsWithSDKAccounts:(GSASDKAccounts *)sdkAccounts {
    NSMutableArray *ret = NSMutableArray.array;
    
    for (int i = 0; i < sdkAccounts->n; i++) {
        GSASDKAccount *sdkAccount = sdkAccounts->data[i];
        GSAAccount *account = [self.alloc initWithSDKAccount:sdkAccount];
        [ret addObject:account];
    }
    
    return ret;
}

@end










@implementation NSArray (GSAAccount)

+ (instancetype)gsaAccountsWithSDKAccounts:(GSASDKAccounts *)sdkAccounts {
    NSMutableArray *ret = NSMutableArray.array;
    
    for (int i = 0; i < sdkAccounts->n; i++) {
        GSASDKAccount *sdkAccount = sdkAccounts->data[i];
        GSAAccount *account = [GSAAccount.alloc initWithSDKAccount:sdkAccount];
        [ret addObject:account];
    }
    
    return ret;
}

@end
