//
//  GSADomain.h
//  GSAuth
//
//  Created by Dan Kalinin on 12/12/19.
//

#import "GSAMain.h"
#import "GSAToken.h"

@class GSAAccount;
@class GSAController;










@interface GSADomain : NSEObject

@property (readonly) NSString *id;
@property (readonly) NSString *name;
@property (readonly) GSAToken *token;
@property (readonly) GSAAccount *account;
@property (readonly) GSAAccount *owner;
@property (readonly) NSArray<GSAController *> *controllers;
@property (readonly) NSArray<GSAAccount *> *accounts;

- (instancetype)initWithSDKDomain:(GSASDKDomain *)sdkDomain;

@end










@interface NSArray (GSADomain)

+ (instancetype)gsaDomainsWithSDKDomains:(GSASDKDomains *)sdkDomains;

@end
