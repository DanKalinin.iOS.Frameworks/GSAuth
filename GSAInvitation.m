//
//  GSAInvitation.m
//  GSAuth
//
//  Created by Dan Kalinin on 4/1/20.
//

#import "GSAInvitation.h"
#import "GSADomain.h"
#import "GSADatabase.h"










@implementation GSAInvitation {
    NSString *__domain;
    GSADomain *_domain;
}

- (GSADomain *)domain {
    if (_domain == nil) {
        _domain = [GSADatabase.shared domain:__domain];
    }
    
    return _domain;
}

- (instancetype)initWithSDKInvitation:(GSASDKInvitation *)sdkInvitation {
    self = super.init;
    
    if (self != nil) {
        _id = @(sdkInvitation->id);
        _name = @(sdkInvitation->name);
    }
    
    return self;
}

@end










@implementation NSArray (GSAInvitation)

+ (instancetype)gsaInvitationsWithSDKInvitations:(GSASDKInvitations *)sdkInvitations {
    NSMutableArray *ret = NSMutableArray.array;
    
    for (int i = 0; i < sdkInvitations->n; i++) {
        GSASDKInvitation *sdkInvitation = sdkInvitations->data[i];
        GSAInvitation *invitation = [GSAInvitation.alloc initWithSDKInvitation:sdkInvitation];
        [ret addObject:invitation];
    }
    
    return ret;
}

@end
