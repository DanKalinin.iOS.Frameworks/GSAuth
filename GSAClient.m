//
//  GSAClient.m
//  Pods
//
//  Created by Dan Kalinin on 12/12/19.
//

#import "GSAClient.h"

@implementation GSAClient

- (instancetype)init {
    self = super.init;
    
    if (self != nil) {
        gsa_sdk_run();
    }
    
    return self;
}

static void GSAClientPhoneCheckCallback(int available, GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientPhoneCheckCompletion completion = CFBridgingRelease(sdkCompletion);
    GSAError *error = nil;
    
    if (sdkError != NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, available, error);
    }];
}

- (void)phoneCheck:(NSString *)phone completion:(GSAClientPhoneCheckCompletion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_phone_check((char *)phone.UTF8String, GSAClientPhoneCheckCallback, (void *)sdkCompletion);
}

static void GSAClientAccountLoginCallback(GSASDKAccount *sdkAccount, GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientAccountLoginCompletion completion = CFBridgingRelease(sdkCompletion);
    GSAAccount *account = nil;
    GSAError *error = nil;
    
    if (sdkAccount == NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    } else {
        account = [GSAAccount.alloc initWithSDKAccount:sdkAccount];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, account, error);
    }];
}

- (void)accountLogin:(NSString *)phone password:(NSString *)password completion:(GSAClientAccountLoginCompletion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_account_login((char *)phone.UTF8String, (char *)password.UTF8String, GSAClientAccountLoginCallback, (void *)sdkCompletion);
}

static void GSAClientAccountCreateCallback(GSASDKAccount *sdkAccount, GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientAccountCreateCompletion completion = CFBridgingRelease(sdkCompletion);
    GSAAccount *account = nil;
    GSAError *error = nil;
    
    if (sdkAccount == NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    } else {
        account = [GSAAccount.alloc initWithSDKAccount:sdkAccount];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, account, error);
    }];
}

- (void)accountCreate:(NSString *)phone password:(NSString *)password name:(NSString *)name completion:(GSAClientAccountCreateCompletion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_account_create((char *)phone.UTF8String, (char *)password.UTF8String, (char *)name.UTF8String, GSAClientAccountCreateCallback, (void *)sdkCompletion);
}

static void GSAClientAccountVerifyCallback(GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientAccountVerifyCompletion completion = CFBridgingRelease(sdkCompletion);
    GSAError *error = nil;
    
    if (sdkError != NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, error);
    }];
}

- (void)accountVerify:(NSString *)phone code:(NSString *)code completion:(GSAClientAccountVerifyCompletion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_account_verify((char *)phone.UTF8String, (char *)code.UTF8String, GSAClientAccountVerifyCallback, (void *)sdkCompletion);
}

static void GSAClientAccountVerifyV1Callback(GSASDKAccount *sdkAccount, GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientAccountVerifyV1Completion completion = CFBridgingRelease(sdkCompletion);
    GSAAccount *account = nil;
    GSAError *error = nil;
    
    if (sdkAccount == NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    } else {
        account = [GSAAccount.alloc initWithSDKAccount:sdkAccount];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, account, error);
    }];
}

- (void)accountVerifyV1:(NSString *)phone code:(NSString *)code completion:(GSAClientAccountVerifyV1Completion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_account_verify_v1((char *)phone.UTF8String, (char *)code.UTF8String, GSAClientAccountVerifyV1Callback, (void *)sdkCompletion);
}

static void GSAClientAccountRequestCodeCallback(GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientAccountRequestCodeCompletion completion = CFBridgingRelease(sdkCompletion);
    GSAError *error = nil;
    
    if (sdkError != NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, error);
    }];
}

- (void)accountRequestCode:(NSString *)phone completion:(GSAClientAccountRequestCodeCompletion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_account_request_code((char *)phone.UTF8String, GSAClientAccountRequestCodeCallback, (void *)sdkCompletion);
}

static void GSAClientAccountRequestCodeV1Callback(GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientAccountRequestCodeV1Completion completion = CFBridgingRelease(sdkCompletion);
    GSAError *error = nil;
    
    if (sdkError != NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, error);
    }];
}

- (void)accountRequestCodeV1:(NSString *)phone completion:(GSAClientAccountRequestCodeV1Completion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_account_request_code_v1((char *)phone.UTF8String, GSAClientAccountRequestCodeV1Callback, (void *)sdkCompletion);
}

static void GSAClientAccountPasswordUpdateCallback(GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientAccountPasswordUpdateCompletion completion = CFBridgingRelease(sdkCompletion);
    GSAError *error = nil;
    
    if (sdkError != NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, error);
    }];
}

- (void)accountPasswordUpdate:(NSString *)phone password:(NSString *)password completion:(GSAClientAccountPasswordUpdateCompletion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_account_password_update((char *)phone.UTF8String, (char *)password.UTF8String, GSAClientAccountPasswordUpdateCallback, (void *)sdkCompletion);
}

static void GSAClientAccountGetCallback(GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientAccountGetCompletion completion = CFBridgingRelease(sdkCompletion);
    GSAError *error = nil;
    
    if (sdkError != NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, error);
    }];
}

- (void)accountGet:(NSString *)phone completion:(GSAClientAccountGetCompletion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_account_get((char *)phone.UTF8String, GSAClientAccountGetCallback, (void *)sdkCompletion);
}

static void GSAClientAccountDomainCreateCallback(GSASDKDomain *sdkDomain, GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientAccountDomainCreateCompletion completion = CFBridgingRelease(sdkCompletion);
    GSADomain *domain = nil;
    GSAError *error = nil;
    
    if (sdkDomain == NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    } else {
        domain = [GSADomain.alloc initWithSDKDomain:sdkDomain];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, domain, error);
    }];
}

- (void)accountDomainCreate:(NSString *)phone name:(NSString *)name controllers:(NSArray<GSAController *> *)controllers completion:(GSAClientAccountDomainCreateCompletion)completion {
    GSASDKController elements[controllers.count];
    GSASDKController *pointers[controllers.count];
    for (NSUInteger i = 0; i < controllers.count; i++) {
        pointers[i] = &elements[i];
    }
    
    GSASDKControllers sdkControllers = {0};
    sdkControllers.data = pointers;
    sdkControllers.n = (int)controllers.count;
    [controllers gsaToSDKControllers:&sdkControllers];
    
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_account_domain_create((char *)phone.UTF8String, (char *)name.UTF8String, &sdkControllers, GSAClientAccountDomainCreateCallback, (void *)sdkCompletion);
}

static void GSAClientAccountDomainVerifyCallback(GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientAccountDomainVerifyCompletion completion = CFBridgingRelease(sdkCompletion);
    GSAError *error = nil;
    
    if (sdkError != NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, error);
    }];
}

- (void)accountDomainVerify:(NSString *)phone id:(NSString *)id completion:(GSAClientAccountDomainVerifyCompletion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_account_domain_verify((char *)phone.UTF8String, (char *)id.UTF8String, GSAClientAccountDomainVerifyCallback, (void *)sdkCompletion);
}

static void GSAClientAccountControllerVerifyCallback(GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientAccountControllerVerifyCompletion completion = CFBridgingRelease(sdkCompletion);
    GSAError *error = nil;
    
    if (sdkError != NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, error);
    }];
}

- (void)accountControllerVerify:(NSString *)phone mac:(NSString *)mac completion:(GSAClientAccountControllerVerifyCompletion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_account_controller_verify((char *)phone.UTF8String, (char *)mac.UTF8String, GSAClientAccountControllerVerifyCallback, (void *)sdkCompletion);
}

static void GSAClientAccountInvitationAcceptCallback(GSASDKDomain *sdkDomain, GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientAccountInvitationAcceptCompletion completion = CFBridgingRelease(sdkCompletion);
    GSADomain *domain = nil;
    GSAError *error = nil;
    
    if (sdkDomain == NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    } else {
        domain = [GSADomain.alloc initWithSDKDomain:sdkDomain];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, domain, error);
    }];
}

- (void)accountInvitationAccept:(NSString *)phone id:(NSString *)id completion:(GSAClientAccountInvitationAcceptCompletion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_account_invitation_accept((char *)phone.UTF8String, (char *)id.UTF8String, GSAClientAccountInvitationAcceptCallback, (void *)sdkCompletion);
}

static void GSAClientAccountDomainDeleteCallback(GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientAccountDomainDeleteCompletion completion = CFBridgingRelease(sdkCompletion);
    GSAError *error = nil;
    
    if (sdkError != NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, error);
    }];
}

- (void)accountDomainDelete:(NSString *)phone id:(NSString *)id completion:(GSAClientAccountDomainDeleteCompletion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_account_domain_delete((char *)phone.UTF8String, (char *)id.UTF8String, GSAClientAccountDomainDeleteCallback, (void *)sdkCompletion);
}

static void GSAClientAccountRefreshCallback(GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientAccountRefreshCompletion completion = CFBridgingRelease(sdkCompletion);
    GSAError *error = nil;
    
    if (sdkError != NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, error);
    }];
}

- (void)accountRefresh:(NSString *)phone completion:(GSAClientAccountRefreshCompletion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_account_refresh((char *)phone.UTF8String, GSAClientAccountRefreshCallback, (void *)sdkCompletion);
}

static void GSAClientAccountLogoutCallback(GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientAccountLogoutCompletion completion = CFBridgingRelease(sdkCompletion);
    GSAError *error = nil;
    
    if (sdkError != NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, error);
    }];
}

- (void)accountLogout:(NSString *)phone completion:(GSAClientAccountLogoutCompletion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_account_logout((char *)phone.UTF8String, GSAClientAccountLogoutCallback, (void *)sdkCompletion);
}

static void GSAClientDomainDeleteCallback(GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientDomainDeleteCompletion completion = CFBridgingRelease(sdkCompletion);
    GSAError *error = nil;
    
    if (sdkError != NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, error);
    }];
}

- (void)account:(NSString *)phone domainDelete:(NSString *)id completion:(GSAClientDomainDeleteCompletion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_domain_delete((char *)phone.UTF8String, (char *)id.UTF8String, GSAClientDomainDeleteCallback, (void *)sdkCompletion);
}

static void GSAClientDomainRenameCallback(GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientDomainRenameCompletion completion = CFBridgingRelease(sdkCompletion);
    GSAError *error = nil;
    
    if (sdkError != NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, error);
    }];
}

- (void)account:(NSString *)phone domainRename:(NSString *)id name:(NSString *)name completion:(GSAClientDomainRenameCompletion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_domain_rename((char *)phone.UTF8String, (char *)id.UTF8String, (char *)name.UTF8String, GSAClientDomainRenameCallback, (void *)sdkCompletion);
}

static void GSAClientDomainGetCodeCallback(char *sdkCode, GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientDomainGetCodeCompletion completion = CFBridgingRelease(sdkCompletion);
    NSString *code = nil;
    GSAError *error = nil;
    
    if (sdkCode == NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    } else {
        code = @(sdkCode);
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, code, error);
    }];
}

- (void)account:(NSString *)phone getCode:(NSString *)id completion:(GSAClientDomainGetCodeCompletion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_domain_get_code((char *)phone.UTF8String, (char *)id.UTF8String, GSAClientDomainGetCodeCallback, (void *)sdkCompletion);
}

static void GSAClientDomainControllersAddCallback(GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientDomainControllersAddCompletion completion = CFBridgingRelease(sdkCompletion);
    GSAError *error = nil;
    
    if (sdkError != NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, error);
    }];
}

- (void)account:(NSString *)phone domainControllersAdd:(NSString *)id controllers:(NSArray<GSAController *> *)controllers completion:(GSAClientDomainControllersAddCompletion)completion {
    GSASDKController elements[controllers.count];
    GSASDKController *pointers[controllers.count];
    for (NSUInteger i = 0; i < controllers.count; i++) {
        pointers[i] = &elements[i];
    }
    
    GSASDKControllers sdkControllers = {0};
    sdkControllers.data = pointers;
    sdkControllers.n = (int)controllers.count;
    [controllers gsaToSDKControllers:&sdkControllers];
    
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_domain_controllers_add((char *)phone.UTF8String, (char *)id.UTF8String, &sdkControllers, GSAClientDomainControllersAddCallback, (void *)sdkCompletion);
}

static void GSAClientDomainControllerDeleteCallback(GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientDomainControllerDeleteCompletion completion = CFBridgingRelease(sdkCompletion);
    GSAError *error = nil;
    
    if (sdkError != NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, error);
    }];
}

- (void)account:(NSString *)phone domainControllerDelete:(NSString *)id mac:(NSString *)mac completion:(GSAClientDomainControllerDeleteCompletion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_domain_controller_delete((char *)phone.UTF8String, (char *)id.UTF8String, (char *)mac.UTF8String, GSAClientDomainControllerDeleteCallback, (void *)sdkCompletion);
}

static void GSAClientDomainInvitationCreateCallback(GSASDKInvitation *sdkInvitation, GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientDomainInvitationCreateCompletion completion = CFBridgingRelease(sdkCompletion);
    GSAInvitation *invitation = nil;
    GSAError *error = nil;
    
    if (sdkInvitation == NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    } else {
        invitation = [GSAInvitation.alloc initWithSDKInvitation:sdkInvitation];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, invitation, error);
    }];
}

- (void)account:(NSString *)phone domainInvitationCreate:(NSString *)id name:(NSString *)name expiration:(NSUInteger)expiration access:(NSUInteger)access completion:(GSAClientDomainInvitationCreateCompletion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_domain_invitation_create((char *)phone.UTF8String, (char *)id.UTF8String, (char *)name.UTF8String, (int)expiration, (int)access, GSAClientDomainInvitationCreateCallback, (void *)sdkCompletion);
}

static void GSAClientDomainAccountDeleteCallback(GSASDKError *sdkError, void *sdkCompletion) {
    GSAClientDomainAccountDeleteCompletion completion = CFBridgingRelease(sdkCompletion);
    GSAError *error = nil;
    
    if (sdkError != NULL) {
        error = [GSAError.alloc initWithSDKError:sdkError];
    }
    
    [NSRunLoop.mainRunLoop performBlock:^{
        NSE_CALL_FUNCTION(completion, error);
    }];
}

- (void)account:(NSString *)phone domainGuestDelete:(NSString *)id guestPhone:(NSString *)guestPhone completion:(GSAClientDomainAccountDeleteCompletion)completion {
    CFTypeRef sdkCompletion = CFBridgingRetain(completion);
    gsa_sdk_domain_account_delete((char *)phone.UTF8String, (char *)id.UTF8String, (char *)guestPhone.UTF8String, GSAClientDomainAccountDeleteCallback, (void *)sdkCompletion);
}

+ (instancetype)shared {
    static GSAClient *ret = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ret = self.new;
    });
    
    return ret;
}

@end
